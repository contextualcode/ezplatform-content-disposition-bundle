<?php

namespace ContextualCode\EzPlatformContentDispositionBundle\Controller;

use ContextualCode\EzPlatformContentDispositionBundle\Service\ContentDispositionService;
use Ibexa\Bundle\IO\BinaryStreamResponse;
use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\Values\Content\Field;
use Ibexa\Core\Base\Exceptions\NotFoundException;
use Ibexa\Core\Helper\TranslationHelper;
use Ibexa\Core\IO\IOServiceInterface;
use Ibexa\Core\MVC\Symfony\Controller\Content\DownloadController as BaseDownloadController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DownloadController extends BaseDownloadController
{
    /** @var \Ibexa\Contracts\Core\Repository\ContentService */
    private $contentService;

    /** @var \Ibexa\Core\IO\IOServiceInterface */
    private $ioService;

    /** @var \Ibexa\Core\Helper\TranslationHelper */
    private $translationHelper;

    /** @var ContentDispositionService */
    protected $contentDispositionService;

    public function __construct(
        ContentService $contentService,
        IOServiceInterface $ioService,
        TranslationHelper $translationHelper,
        ContentDispositionService $contentDispositionService
    ) {
        $this->contentService = $contentService;
        $this->ioService = $ioService;
        $this->translationHelper = $translationHelper;
        parent::__construct($contentService, $ioService, $translationHelper);
        $this->contentDispositionService = $contentDispositionService;
    }

    public function downloadBinaryFileLegacyAction(int $contentId, string $attributeId, string $fieldIdentifier, string $filename, Request $request): BinaryStreamResponse
    {
        return $this->downloadBinaryFileAction($contentId, $fieldIdentifier, $filename, $request);
    }

    /**
     * @param mixed $contentId ID of a valid Content
     * @param string $fieldIdentifier Field Definition identifier of the Field the file must be downloaded from
     * @param string $filename
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Ibexa\Bundle\IO\BinaryStreamResponse
     *
     * @throws \Ibexa\Core\Base\Exceptions\NotFoundException
     * @throws \Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException
     * @throws \Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException
     */
    public function downloadBinaryFileAction(int $contentId, string $fieldIdentifier, string $filename, Request $request): BinaryStreamResponse
    {
        if ($request->query->has('version')) {
            $version = (int) $request->query->get('version');
            if ($version <= 0) {
                throw new NotFoundException('File', $filename);
            }
            $content = $this->contentService->loadContent($contentId, null, $version);
        } else {
            $content = $this->contentService->loadContent($contentId);
        }

        if ($content->contentInfo->isTrashed()) {
            throw new NotFoundException('File', $filename);
        }

        // BC layer
        if (is_numeric($fieldIdentifier)) {
            foreach ($content->getFields() as $field) {
                if ($field->id == $fieldIdentifier) {
                    // $response = new RedirectResponse(
                    //     str_replace(
                    //         "/{$fieldIdentifier}/", "/{$field->fieldDefIdentifier}/",
                    //         $request->getRequestUri()
                    //     ),
                    //     RedirectResponse::HTTP_MOVED_PERMANENTLY
                    // );
                    // $this->contentDispositionService->tagPublicDownload($response, $contentId);

                    // return $response;
                    $fieldIdentifier = $field->fieldDefIdentifier;
                    break;
                }
            }
        }

        $field = $this->translationHelper->getTranslatedField(
            $content,
            $fieldIdentifier,
            $request->query->has('inLanguage') ? $request->query->get('inLanguage') : null
        );
        if (!$field instanceof Field) {
            throw new NotFoundHttpException('File not found.');
            /*throw new InvalidArgumentException(
                "'{$fieldIdentifier}' field not present on content #{$content->contentInfo->id} '{$content->contentInfo->name}'"
            );*/
        }

        $binaryFile = $this->ioService->loadBinaryFile($field->value->id);
        $contentDisposition = $this->contentDispositionService->getContentDisposition($this->ioService->getMimeType($field->value->id));
        $response = new BinaryStreamResponse($binaryFile, $this->ioService);
        $response->setContentDisposition($contentDisposition, $filename);

        $this->contentDispositionService->tagPublicDownload($response, (int)$contentId);

        return $response;
    }
}
