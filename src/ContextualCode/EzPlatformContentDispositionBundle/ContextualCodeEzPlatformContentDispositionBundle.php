<?php

namespace ContextualCode\EzPlatformContentDispositionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeEzPlatformContentDispositionBundle extends Bundle
{
    protected $name = 'ContextualCodeEzPlatformContentDispositionBundle';
}
