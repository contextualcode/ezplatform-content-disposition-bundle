<?php

namespace ContextualCode\EzPlatformContentDispositionBundle\Service;

use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;
use Ibexa\Contracts\HttpCache\Handler\ContentTagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\EventListener\SessionListener;

class ContentDispositionService
{
    const CONTENT_DISPOSITION_DEFAULT = ResponseHeaderBag::DISPOSITION_ATTACHMENT;
    const PARAM_CONTENT_DISPOSITION_DEFAULT = 'download_content_disposition.default';
    const PARAM_CONTENT_DISPOSITION_MIME_TYPE = 'download_content_disposition.mimetype';
    const PARAM_CONTENT_DOWNLOAD_PUBLIC = 'download_content_disposition.public_download';
    const PARAM_CONTENT_DOWNLOAD_TTL = 'download_content_disposition.download_file_ttl';

    /**
     * @var ConfigResolverInterface
     */
    protected $configResolver;

    /** @var ContentTagInterface */
    private $tagger;

    public function __construct(ConfigResolverInterface $configResolver, ContentTagInterface $tagger)
    {
        $this->configResolver = $configResolver;
        $this->tagger = $tagger;
    }

    public function getContentDisposition(string $mimeType)
    {
        $defaultContentDisposition = self::CONTENT_DISPOSITION_DEFAULT;
        if ($this->configResolver->hasParameter(self::PARAM_CONTENT_DISPOSITION_DEFAULT)) {
            $defaultContentDisposition = $this->configResolver->getParameter(self::PARAM_CONTENT_DISPOSITION_DEFAULT);
        }

        if ($this->configResolver->hasParameter(self::PARAM_CONTENT_DISPOSITION_MIME_TYPE)) {
            $mimeTypeContentDispositions = $this->configResolver->getParameter(self::PARAM_CONTENT_DISPOSITION_MIME_TYPE);
        }

        return $mimeTypeContentDispositions[$mimeType] ?? $defaultContentDisposition;
    }

    public function tagPublicDownload(Response $response, int $contentId)
    {
        if (
            !$this->configResolver->hasParameter(self::PARAM_CONTENT_DOWNLOAD_PUBLIC)
            || !$this->configResolver->getParameter(self::PARAM_CONTENT_DOWNLOAD_PUBLIC)
        ) {
            return;
        }

        $ttl = $this->configResolver->hasParameter(self::PARAM_CONTENT_DOWNLOAD_TTL)
            ? $this->configResolver->getParameter(self::PARAM_CONTENT_DOWNLOAD_TTL)
            : 0;
        if (!$ttl && $this->configResolver->hasParameter('content.default_ttl')) {
            $ttl = $this->configResolver->getParameter('content.default_ttl');
        }
        if (!$ttl) {
            return;
        }

        if ($contentId) {
            $this->tagger->addContentTags([$contentId]);
        }

        $response
            ->setSharedMaxAge($ttl)
            ->setVary(['X-User-Context-Hash']);
        // header to avoid Symfony SessionListener overwriting the response to private
        $response->headers->set(SessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 1);
    }
}
