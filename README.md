# ContextualCode\EzPlatformContentDispositionBundle

This bundle gives the ability to customize content dispositions (inline or attachment) for downloads.

You can specify the default content disposition, and content dispositions per mime type.


## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/ezplatform-content-disposition-bundle
```
- Enable this bundle in `config/bundles.php` by adding this line:

```php
    return [
        ...,
        ContextualCode\EzPlatformContentDispositionBundle\ContextualCodeEzPlatformContentDispositionBundle::class => ['all' => true],
    ];
```

Add this to your `routes.yaml`:
```yaml
content_disposition:
  resource: '@ContextualCodeEzPlatformContentDispositionBundle/Resources/config/routing.yml'
```

## Usage

In some `parameters` block, for example in `config/services.yaml`, you can specify:

- A default content disposition:
  ```yml
  ibexa.site_access.config.global.download_content_disposition.default: inline
  ```

- A content disposition per mimetype:
  ```yml
  ibexa.site_access.config.global.download_content_disposition.mimetype:
    "application/pdf": inline
    "application/msword": attachment
  ```

- Make files `cache-control: public, s-maxage=%download_content_disposition.download_file_ttl%`
and vary by user context hash (default: `false`).
  ```yml
  ibexa.site_access.config.default.download_content_disposition.public_download: true
  ibexa.site_access.config.default.download_content_disposition.download_file_ttl: 86400
  ```
> if `download_content_disposition.download_file_ttl` setting not defined, `content.default_ttl` will be used.
